var 
	gulp				= require('gulp'),
	clean 			= require('gulp-clean'),
	svgSprite		= require('gulp-svg-sprite');

// More complex configuration example
// config					= {
// 	shape				: {
// 		dimension		: {			// Set maximum dimensions
// 			maxWidth	: 32,
// 			maxHeight	: 32
// 		},
// 		spacing			: {			// Add padding
// 			padding		: 10
// 		},
// 		dest			: 'dest/intermediate-svg'	// Keep the intermediate files
// 	},
// 	mode				: {
// 		view			: {			// Activate the «view» mode
// 			bust		: false,
// 			render		: {
// 				scss	: true		// Activate Sass output (with default options)
// 			}
// 		},
// 		symbol			: true		// Activate the «symbol» mode
// 	}
// };

// «symbol» sprite with CSS stylesheet resource 
 
// var config					= {
//     mode					: {
//         inline				: true,		// Prepare for inline embedding 
//         symbol				: true		// Create a «symbol» sprite 
//     }
// }


var config					= {
	mode					: {
		defs				: false,
		symbol				: true,
		stack				: false
	}
}

gulp.task('clean', function () {
  return gulp.src('dest')
    .pipe(clean());
});

gulp.task('svg-sprites',['clean'], function() {
   gulp.src('dev/setting-icon/svg-icon/*.svg')
	.pipe(svgSprite( config ))
	.pipe(gulp.dest('dest'));
});

gulp.task('easystore-svg',['clean'], function() {
	gulp.src('dev/easystore-icon/iconset/*.svg')
 .pipe(svgSprite( config ))
 .pipe(gulp.dest('dest/easystore-icon'));
});




gulp.task('default', ['svg-sprites'])